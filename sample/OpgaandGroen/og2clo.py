"""
Samenvatting van de Opgaand Groen indicator van de Landschapsmonitor tbv de CLO
Hans Roelofsen, WEnR, 30 sept 2021
"""

import datetime
import pandas as pd
import geopandas as gp
import os

og_dir = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\OpgaandGroen\a_Geodata\leveringPPK20210915'
og_gdb = {2016: 'MonitoringsbronOpgaandGroen2016_dd20210831.gdb',
          2018: 'MonitoringsbronOpgaandGroen2018_dd20210831.gdb',
          2019: 'MonitoringsbronOpgaandGroen2019_dd20210720.gdb',
          2021: 'MonitoringsbronOpgaandGroen2021_dd20210720.gdb'}
prov_src = {'metwater': r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\provincies\incl_water\provincies_2018.shp',
            'gegeneraliseerd': r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\provincies\gegeneraliseerd\CBS_provincies_2020_gegeneraliseerd.shp'}

years = [2016, 2018, 2019, 2021]
prov_choice = 'metwater'
out_dir = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\OpgaandGroen\h_compendium_teksten'

# read data
provincies = gp.read_file(prov_src['metwater']).set_index('provincien', drop=False)

for year in years:
    print('year {}'.format(year))
    holder = []
    excel_out = r'OpgaandGroenCompendiumCijfers{}.xlsx'.format(year)

    for prov in provincies.provincien:
        print('  doing {}'.format(prov))

        lijn = gp.read_file(os.path.join(og_dir, og_gdb[year]), layer='Lijnen', bbox=provincies.loc[prov, 'geometry'])
        print('    {} lijnen'.format(lijn.shape[0]))

        vlak = gp.read_file(os.path.join(og_dir, og_gdb[year]), layer='Vlakken', bbox=provincies.loc[prov, 'geometry'])
        print('    {} vlakken'.format(vlak.shape[0]))

        punt = gp.read_file(os.path.join(og_dir, og_gdb[year]), layer='Punten', bbox=provincies.loc[prov, 'geometry'])
        print('    {} punten'.format(punt.shape[0]))

        out = pd.Series({'km_lijnvormig': lijn.SHAPE_Length.divide(1000).sum(),
                         'hectare_vlakvormig': vlak.SHAPE_Area.divide(10000).sum(),
                         'aantal_puntvormig': punt.shape[0]}, name=prov)
        holder.append(out)
        del out
        del lijn
        del vlak
        del punt

    out = pd.concat(holder, axis=1).T.merge(provincies['area_ha'], right_index=True, left_index=True, how='outer').fillna(0)


    print('  writing output')
    with pd.ExcelWriter(os.path.join(out_dir, excel_out), mode='w') as writer:
        metadata = {'sourcedata': os.path.join(og_dir, og_gdb[year]),
                    'yaer': year,
                    'provincie source': prov_src[prov_choice],
                    'made by': os.environ.get('USERNAME'),
                    'made when': datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S'),
                    'made with': 'https://git.wur.nl/roelo008/landschapsmonitor/-/blob/master/sample/OpgaandGroen/og2clo.py'}
        pd.DataFrame(data=metadata, index=[0]).T.to_excel(writer, sheet_name='Metadata', index=True)

        out.to_excel(writer, sheet_name='Statistiek_OpgaandGroen', index=True)






