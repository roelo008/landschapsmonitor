import os

def replace_hdr(dir, hdrname):
    """
    replace a header file with standard fromat
    :param dir:
    :param f:
    :return:
    """

    hdr = "ncols         10800\n" \
          "nrows         13000\n" \
          "xllcorner     10000\n" \
          "yllcorner     300000\n" \
          "cellsize      25\n" \
          "NODATA_value  -9999\n" \
          "byteorder     LSBFIRST"

    with open(os.path.join(dir, hdrname), 'w') as f:
        f.write(hdr)


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help='directory', type=str)
    parser.add_argument('hdr', help='', type=str)
    args = parser.parse_args()

    replace_hdr(args.dir, args.hdr)

