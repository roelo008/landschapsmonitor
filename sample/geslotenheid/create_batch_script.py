"""
Genereer een BATCH script, waarmee een OPGAANDE ELEMENTEN kaart wordt gemaakt voor een JAARGANG
Hans Roelofsen, WEnR, september 2021
"""

import sys
import yaml

with open(r'brondata.yml') as f:
    bd = yaml.load(f, Loader=yaml.FullLoader)
    bd = bd['brondata']

def create_batch_script(year):
    """
    verzamel brondata behorende bij het JAAR en plug deze brondata in het dummy batch script
    :param year: jaargang
    :return: .\prep_opgaande_elementen_<year>.bat
    """

    try:
        replace_dict = {
            'REPLACE_YEAR': str(year),
            'REPLACE_OG_GDB': bd[year]['groen']['gdb'],
            'REPLACE_BB_GDB': bd[year]['bebouwing']['gdb'],
            'REPLACE_BB_FC': bd[year]['bebouwing']['vlak'],
            'REPLACE_T10_GDB': bd[year]['top10']['gdb'],
            'REPLACE_SCRATCH': r'c:\apps\temp_geodata\openheid21\scratch\1_1_{}'.format(year)}

    except KeyError as e:
        print('invalid year: {}'.format(e))
        sys.exit(1)

    out_file = 'prep_opgaande_elementen_{}.bat'.format(year)

    with open('prep_opgaande_elementen_dummy.bat', 'r') as template:
        template_data = template.read()

    for k, v in replace_dict.items():
        template_data = template_data.replace(k, v)

    with open('./{}'.format(out_file), 'w') as dest:
        dest.write(template_data)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('year', type=int, help='for which year? peildatum 31 12 YEAR',
                        choices=[2022, 2020, 2018, 2017, 2015])
    args = parser.parse_args()

    create_batch_script(args.year)


