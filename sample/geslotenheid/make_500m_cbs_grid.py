"""
Create grid of 500m cells, mirroring design of https://www.cbs.nl/nl-nl/longread/diversen/2021/statistische-gegevens-per-vierkant-en-postcode-2020-2019-2018/2-technische-gegevens
Hans Roelofsen, WEnR,
"""

import sys
sys.path.append(r'c:/apps/proj_code/benb_utils')
import benb
import geopandas as gp
import numpy as np

# extent in meters
left = 10000
bottom = 300000
right = 280000
top = 625000
size = 500
ncols = int(((right - left)/size))
nrows = int(((top - bottom)/size))

sq = benb.gen_squares(x_ul=left, y_ul=top, nrow=nrows, ncol=ncols, size=500)

# Add CBS codering
sq['C28992R500'] = ['E{:04d}N{:04d}'.format(int(e), int(n)) for e, n in zip(np.divide(sq.geometry.bounds.minx, 100),
                                                                            np.divide(sq.geometry.bounds.miny, 100))]

# Spation join to provincies
provs = gp.read_file(r'c:\Users\roelo008\OneDrive - WageningenUR\b_geodata\provincies_2018\poly\provincies_2018.shp')
foo = gp.sjoin(sq, provs.loc[:, ['provincien', 'geometry']], how='left')

# Drop grid cells zonder Provincie
foo.dropna(subset=['provincien'], inplace=True)

# Some hokken may be coupled to > 1 provincie. Undo this
if len(set(foo.ID)) < foo.shape[0]:
    foo.drop_duplicates(subset=['ID'], inplace=True)

foo.drop(labels=['ID', 'index_right', 'size'], axis=1).to_file(r'c:\apps\temp_geodata\CBS_500m_grid.shp')
# sq.to_file(r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\CBS500m_grids\versieWENR\CBS_500m_grid.shp')