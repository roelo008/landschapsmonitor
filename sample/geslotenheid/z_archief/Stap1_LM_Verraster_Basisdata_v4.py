# ---------------------------------------------------------------------------
# Stap1_LM_Verraster_Basisdata_v4.py
# Created on: 2020-05-04
# Description: 
# ---------------------------------------------------------------------------

# Import arcpy module
import arcpy
import datetime
from arcpy import env
from arcpy.sa import *

'''Create timestamp. All output names will be extended with it, so previous output will never be overwritten. '''
t0 = datetime.datetime.now()
date_label = 'time{0}'.format(t0.strftime("%Y%m%d%H%M%S"))
print("started @ {0}".format(t0.strftime("%Y%m%d%H%M%S")))

'''Set Geoprocessing environments and settings'''
env.overwriteOutput = True
env.parallelProcessingFactor = "50%"
arcpy.CheckOutExtension("spatial")
env.scratchWorkspace = "c:\\apps\\temp_geodata\\scratch.gdb"

'''Set processing extent. Several extents that can be used while testing the script'''
#Netherlands
env.extent = "10000, 300000, 280000, 625000"
#Testing area
#env.extent = "169000, 440000, 174000, 443000,"

'''Set path names'''
temp_pad = "C:\\apps\\temp_geodata\\scratch.gdb"
temp_features = temp_pad+"\\TempFeatures"
out_pad = "C:\\apps\\temp_geodata\\openheid\\LM_BasisRasters.gdb"

'''Input 2018'''
#top10_version = "TOP10NL_2018-sept"
#in_bebouwing_version = "Monitoringsbron2018"

'''Input 2019'''
top10_version = "TOP10NL_2019_Sep"
in_bebouwing_version = "Monitoringsbron2019"

'''Define input paths and dictionaryies'''
top10_pad = "W:\\PROJECTS\\GeoDeskData\\TOP10NL\\"+top10_version+"\\TOP10NL.gdb\\"
bebouwing_pad = "W:\\PROJECTS\\Landschapsmonitor\\Openheid\\c_viewscape_input\\bebouwing\\Monitoringsbron.gdb\\"

#Table with reclass values for blocking land use types.
reclass_table = "W:\\PROJECTS\\Landschapsmonitor\\Openheid\\c_viewscape_input\\coderingen_en_mask\\LM_Coderingen.gdb\\LM_Reclass_Top10"
in_code_field_name = "In_code" #Field name in reclass_table with values to reclass. Field name in rasters that will be reclassed, depends on source.

#Layers_dict is a dictionary with feature classes to process. The value of the key itself will be used in defining the rasteriation process and the output names for the layers.
#If the second part of the layer name = 'Lijn', the feature class should be of type Polyline!
#The dictionary values are lists with: path to the feature class, the name of feature class itself and the name of the field the reclassification will be based upon.
layers_dict = {"Top10_Terrein": [top10_pad, "TERREIN_VLAK", "VISUALISATIECODE"], "Top10_Lijn": [top10_pad, "INRICHTINGSELEMENT_LIJN", "VISUALISATIECODE"],
               "LM_Bebouwing": [bebouwing_pad, in_bebouwing_version, "OBJECT_TYPE"]}
#layers_dict = {"LM_Bebouwing": [bebouwing_pad, in_bebouwing_version, "OBJECT_TYPE"]}
#layers_dict = {"Top10_Terrein": [top10_pad, "TERREIN_VLAK", "VISUALISATIECODE"], "Top10_Lijn": [top10_pad, "INRICHTINGSELEMENT_LIJN", "VISUALISATIECODE"]}

reclass_dict = dict() #Dictionary with actual reclass values. Key = from code, value = to code.
valid_values_dict = dict() #Dictionary with for each feature class a list of values that need to be reclassed. Key = feature class, Value is list of values that need to be
                            #reclassed. Dictionary is used in build a 'MakeFeatureLayer-query
all_values_dict = dict() #Dictionary with for each feature class a list of all known values. Dictionary is used to check if feature class has new, unknown values. If so,
                            #a new table is created, missing values are added and the user will be asked to fill in LM_code.

'''Read reclass values'''
print('reading reclass values')
scur = arcpy.SearchCursor(reclass_table)
for srow in scur:
    if srow.getValue(in_code_field_name).isdigit(): #isdigit returns False for negative values
        in_code = int(srow.getValue(in_code_field_name)) #here in_code will be a positive integer
    elif (srow.getValue(in_code_field_name)[0] == '-') and (srow.getValue(in_code_field_name)[1:].isdigit):
        in_code = int(srow.getValue(in_code_field_name)) #here in_code will be a negative integer
    else:
        in_code = srow.getValue(in_code_field_name) #here in_code will be a string
    #all known values in the reclass table are collected to compare them with the actual values in the input feature class
    if srow.F_class not in all_values_dict:
        all_values_dict[srow.F_class] = [in_code]
    else:
        all_values_dict[srow.F_class].append(in_code)
    if srow.LM_code != -1: #-1 indicates that corresponding feature doesn't need to be reclassed (from in_code to LM_code) and rasterized
        reclass_dict[in_code] = srow.LM_code
        if srow.F_class not in valid_values_dict:
            valid_values_dict[srow.F_class] = [in_code]
        else:
            valid_values_dict[srow.F_class].append(in_code)
del srow
del scur

'''Check for errors'''
#Initialize error check on missing values in input data.
values_missing = False
missing_values_dict = dict() #for each layer (key) missing values are collected in a list (value)

for layer in layers_dict.keys():
    in_path = layers_dict[layer][0]
    fc = layers_dict[layer][1]
    in_field = layers_dict[layer][2]
    in_features = in_path+fc
    print("finding unique values for features", in_features)
    scur = arcpy.SearchCursor(in_features)
    for srow in scur:
        if srow.getValue(in_field) not in all_values_dict[layer]:
            values_missing = True
            #For each layer (feature class), values missing in reclass table are collected.
            if layer in missing_values_dict:
                if srow.getValue(in_field) not in missing_values_dict[layer]:
                    missing_values_dict[layer].append(srow.getValue(in_field))
            else:
                missing_values_dict[layer] = [srow.getValue(in_field)]

'''Report errors'''
if values_missing: #in the above for-loop at least once a value was missing in the reclass table, for one of the feature classes
    print("Process stopped because values are missing in reclass table.")
    #Create a copy of the reclass table and add a record for each missing value.
    new_reclass_table = reclass_table+"_"+date_label
    arcpy.CopyRows_management(reclass_table, new_reclass_table)
    irows = arcpy.InsertCursor(new_reclass_table)
    for l in missing_values_dict.keys():
        print("Missing reclass values in", l, "for", layers_dict[l][2], ":")
        for v in missing_values_dict[l]:
            print(" ",v)
            irow = irows.newRow()
            irow.setValue("In_code", v)
            irow.setValue("F_class", l)
            irows.insertRow(irow)
    del irow
    del irows
    print("Values added to new reclass table", new_reclass_table, "Please fill in reclass values for added codes and activate this new reclass table by removing its timestamp after deleting the current one!")


    '''No errors found'''
else:
    print("Congratulations! No values are missing in reclass table", reclass_table) #only when no values are missing, the feature class will be rasterized
    for layer in layers_dict.keys():
        in_path = layers_dict[layer][0]
        fc = layers_dict[layer][1]
        in_field = layers_dict[layer][2]
        in_features = in_path+fc
        print("selecting features for", in_features)
        vcs = valid_values_dict[layer]
        #build query expression that will be used to select all valid features
        for vc in vcs:
            if type(vc) == str:
                vcq = "'"+vc+"'"
            else:
                vcq = str(vc)
            if vcs.index(vc) == 0:
                query = in_field + " = " + vcq
            else:
                query += " or " + in_field + " = " + vcq
        print(query)
        print("copying features")
        arcpy.MakeFeatureLayer_management(in_features, "Featurelayer", query) #create feature layer using the query expression
        numrecs = arcpy.GetCount_management("Featurelayer")[0]
        print(numrecs, "features selected")
        arcpy.CopyFeatures_management ("Featurelayer", temp_features) #copy the selected features to a local drive location, for better performance and adding an attribute to the attribute table
        print("calculating raster value")
        arcpy.AddField_management(temp_features,"Value", "SHORT")#add value field to hold the reclass value for each feature
        ucur = arcpy.UpdateCursor(temp_features)
        #For each record, calculate the reclass value.
        for urow in ucur:
            urow.Value = reclass_dict[urow.getValue(in_field)]
            ucur.updateRow(urow)
        del urow
        del ucur

        '''Finally rasterization can take place'''
        print ('Rasterizing')
        #Define the out raster names, depending on the layer name.
        if layer.split("_")[0] == "Top10":
            in_top10_version = top10_version.replace('-','_')#in case top10_version has a '-' in its name, it needs to be replaced while it's not allowed in a raster name
            out_rastername = layer+"_"+in_top10_version+"_"+date_label
        elif layer.split("_")[0] == "LM":
            out_rastername = layer+"_"+in_bebouwing_version+"_"+date_label
        #Choose the right rasterization process.
        if layer.split("_")[1] == "Lijn":
            arcpy.PolylineToRaster_conversion (temp_features, "Value", out_pad+"\\"+out_rastername, "MAXIMUM_LENGTH", "#", "2.5")
        else:
            arcpy.PolygonToRaster_conversion (temp_features, "Value", out_pad+"\\"+out_rastername, "CELL_CENTER", "#", "2.5")


del arcpy
a = datetime.datetime.now().replace(microsecond=0)
print ("Finished:",a)
