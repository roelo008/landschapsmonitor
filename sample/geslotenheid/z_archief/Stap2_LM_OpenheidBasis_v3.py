# ---------------------------------------------------------------------------
# Stap2_LM_OpenheidBasis_v2.py
# Created on: Mon February 8 2010 10:00:00 AM
# Description: Creates a basemap for ViewScape
# ---------------------------------------------------------------------------

# 21 juli 2010
# Minimale oppervlakte voor vlakvormig groen stond op GT 38. Moest GT 37 zijn.
# 26 maart 2013
# OpenheidBasis.py herschreven voor arcpy en input via inputfile. Output van dit script getest op Viris2009 en geeft
# exact dezelfde resultaten als script OpenheidBasis.py
# 13 maart 2018
# Aangepast voor nieuwe openheidsberekening

# Import arcpy module
import arcpy, datetime
from arcpy import env
from arcpy.sa import *

'''Defineer timestamp'''
a = datetime.datetime.now().replace(microsecond=0)
print("started:",a)
datelabel = str(a).replace("-", "")
datelabel = datelabel.replace(":", "")
datelabel = datelabel.replace(" ", "time")

# Check out any necessary licenses
arcpy.CheckOutExtension("spatial")

'''Definieer een aantal environment variabelen en directories voor tijdelijke output'''
env.scratchWorkspace = "c:\\apps\\temp_geodata\\scratch.gdb"
temp_dir = "C:\\apps\\temp_geodata\\scratch.gdb"
# Extent voor heel Nederland
env.extent = "10000 300000 280000 625000"
# Extent voor testgebied
#env.extent = "26000, 384000, 42000, 395000"
mask = "W:\\PROJECTS\\Landschapsmonitor\\Openheid\\c_viewscape_input\\coderingen_en_mask\\LM_Coderingen.gdb\\mask_nl"

env.mask = mask
env.parallelProcessingFactor = "50%"
env.overwriteOutput = True

'''Definieer pad voor input'''
in_pad = "C:\\apps\\temp_geodata\\openheid\\LM_BasisRasters.gdb"

'''Input voor 2018'''
##year = "2018"
##bebouwing = in_pad+"\\LM_Bebouwing_Monitoringsbron"+year+"_time20200626152231"
##lijnvormig = in_pad+"\\Top10_Lijn_TOP10NL_"+year+"_sept_time20200629091258"
##terrein = in_pad+"\\Top10_Terrein_TOP10NL_"+year+"_sept_time20200629091258"

'''Input voor 2019'''
year = "2019"
bebouwing = in_pad+"\\LM_Bebouwing_Monitoringsbron"+year+"_time20200812120927"
lijnvormig = in_pad+"\\Top10_Lijn_TOP10NL_"+year+"_Sep_time20200812120927"  
terrein = in_pad+"\\Top10_Terrein_TOP10NL_"+year+"_Sep_time20200812120927"

'''Definieer locaties voor output'''
out_root = "c:\\apps\\temp_geodata\\openheid" # Hier wordt bij elke run een nieuwe gdb aangemaakt waarin de output komt.
print("creating gdb")
newgdb = "Blocking_"+year+"_"+datelabel
arcpy.CreateFileGDB_management(out_root, newgdb)
out_raster_pad = out_root+"\\"+newgdb+".gdb"
out_flt_pad = "c:\\apps\\temp_geodata\\openheid\\flts"
print("created", out_raster_pad)

'''Hier begint het feitelijke rekenwerk aan de drie input-rasrers'''
'''Er wordt vanuitgegaan dat alle waardes groter dan nul zichtbelemmerende objecten vertegenwoordigen'''
# De 'Raster(naam) > 0' levert een 0 (False) of 1 (True) op. Die worden geaggregeerd met een factor 10 op basis van SUM (default) en vermenigvuldigd met
# 6,25 als maat voor de oppervlakte in de cel of met 2,5 als maat voor de lengte in de cel.
print("Finding linear elements from Top10Lijn")
lijngroen_nd = 2.5*(Aggregate((Raster(lijnvormig) > 0), 10))  # Note: SUM is default aggregation method :-)
                                                              # Also Note: (Raster(x) > 0) resulteert in 0/1 raster!
                                                              # Also note 2.5m lenght assumed for lijnvormige elementen
lijngroen = Con(IsNull(lijngroen_nd), 0, lijngroen_nd) #Vervang alle NoData door een 0 (nul).
lijngroen.save(out_raster_pad+"\\Lijngroen")

print("Finding 'forests' from Top10 Terrein")
vlkgroen_nd = 6.25*(Aggregate((Raster(terrein) > 0), 10))  # Raster(terrein) > 0 resulteert in 0/1 raster!!
vlkgroen = Con(IsNull(vlkgroen_nd), 0, vlkgroen_nd)  # Vervang alle NoData door een 0 (nul).
vlkgroen.save(out_raster_pad+"\\VlakGroen")

print("Finding built up area from Bebouwingsindicator")
vlkrood_nd = 6.25*(Aggregate((Raster(bebouwing) > 0), 10))  # Note again: Raster(x) > 0 geeft 0/1 raster!
                                                            # Note: 6.25 * (aantal 2,5m cellen in 25m cel) ==
                                                            # oppervlakte in m2 van de oorspronkelijke 2.5m cellen,
                                                            # 0-625 sq m

vlkrood = Con(IsNull(vlkrood_nd), 0, vlkrood_nd) #Vervang alle NoData door een 0 (nul).
vlkrood.save(out_raster_pad+"\\VlakRood")

# Voor elke cel wordt bepaald hoeveel 'groen' of 'rood' er maximaal wordt aangetroffen in een omgeving van drie bij drie cellen. Dit is nodig om 25m cellen
# grenzend aan volledig gevulde 25m cellen (ie de 25m cellen aan de rand van een bos, die maar deels gevuld zullen zijn
# met 2.5m cellen, maar grenzen aan 25m cellen die 100% gevuld zjin met 2.5m cellen) anders te behandelen dan cellen in
# 'het open veld', ie
print ("calculating focal max vlakvormig groen")
vlk_gr_mx3bij3 = FocalStatistics(vlkgroen, NbrRectangle(3,3,"CELL"), "MAXIMUM", "DATA")
# Dit geeft de MAX waarde in 3x3 zoek gebied
#
# 0 160 625
# 0  X  625  --> Hier is X een cel aan de rand van een bos en de FocalStats geeft dus 625 als uitkomst.
# 0 312 625

print ("calculating focal max vlakvormig rood")
vlk_rd_mx3bij3 = FocalStatistics(vlkrood, NbrRectangle(3,3,"CELL"), "MAXIMUM", "DATA")

# Alle cellen die grenzen aan een volle cel (> 624) moeten voor meer dan de helft gevuld zijn (> 312), om zo weinig mogelijk van de open ruimte
# 'af te snoepen' rondom een massief rood/groen blok. Voor cellen die niet grenzen aan een volle cel ligt het criterium lager. Daarin moet minimaal 37 vierkante meter vlakvormig
# groen zitten (minimale breedte van 3 meter x helft van de doorsnede van een cel). Lijnvormige elementen moeten minimaal de helft
# van de doorsnede van een cel vullen. Daarbij is naar beneden afgerond (12 ipv 13) omdat dat op een aantal plaatsen een beter resultaat gaf.
print ("finding potentieel dicht groen")
vlak_rand_meedoen = ((vlk_gr_mx3bij3 > 624) & (vlkgroen > 312))
vlak_los_meedoen = ((vlk_gr_mx3bij3 <= 624) & (vlkgroen > 37)) 
lijn_meedoen = (lijngroen >= 12)
groendicht0 = (vlak_rand_meedoen | vlak_los_meedoen | lijn_meedoen)

groendicht0 = (((vlk_gr_mx3bij3 > 624) & (vlkgroen > 312)) | ((vlk_gr_mx3bij3 <= 624) & (vlkgroen > 37)) | (lijngroen >= 12))
#                (volle cel nabij      &  zelf > helft vol)|  (geen volle cel nabij)  & (volheidscriteria| (crit lijn opp)
#                                                               ligt niet aan rand       geisol. element)
# Let op: het kan zo zijn dat elk van de 3 criteria (net) **niet** gehaald wordt, maar de arealen van allen tezamen
#         zo groot zijn dat je de cell als dicht zou willen beschouwen.
# Let op: groendicht0 resulteert in een 0(False)/1(True) kaart, wat betekent False=Open True=Gesloten

## Hier begint de Thinning!

# De geisoleerde groene cellen moeten worden opgespoord om ze later terug te kunnen zetten. Ze verdwijnen namelijk na het Thin commando verderop.
# Hiervoor is een FocalStatistics nodig. Is de cel zelf 'groen' en heeft de som van drie bij drie cellen de waarde 1, dan gaat het om een
# geisoleerde cel.
print ("calculating focal sum potentieel groen")
vlk_gr_d03bij3 = FocalStatistics(groendicht0, NbrRectangle(3,3,"CELL"), "SUM", "DATA")

# Diagonale lijnen doorsnijden naast elkaar gelegen cellen waardoor ze meer cellen uit de open ruimte opslokken dan noodzakelijk is.
# Dit kan worden opgelost met het commando Thin. Echter, op plaatsen waar verschillende lijnen bij elkaar komen ontstaan daarbij ongewenste
# effecten als afrondingen. Om dit te voorkomen worden cellen opgezocht die onderdeel zijn van een blokje ven 2 bij 2 cellen en voor die
# cellen wordt het Tin-commando ongedaan gemaakt.

# Ga op zoek naar de cellen die onderdeel zijn van een blokje van 2 bij 2 cellen.
# Schrijf eerst de vier benodigde kernel files voor vier cellen, resp. linksboven, rechtsboven, linksonder en rechtsonder in een omgeving van drie bij
# drie cellen en voer daarna de FocalStatistics uit.
f = open(temp_dir+"\\LB4.txt", 'w')
f.write("3 3\n")
f.write("1 1 0\n")
f.write("1 1 0\n")
f.write("0 0 0\n")
f.close()
f = open(temp_dir+"\\RB4.txt", 'w')
f.write("3 3\n")
f.write("0 1 1\n")
f.write("0 1 1\n")
f.write("0 0 0\n")
f.close()
f = open(temp_dir+"\\LO4.txt", 'w')
f.write("3 3\n")
f.write("0 0 0\n")
f.write("1 1 0\n")
f.write("1 1 0\n")
f.close()
f = open(temp_dir+"\\RO4.txt", 'w')
f.write("3 3\n")
f.write("0 0 0\n")
f.write("0 1 1\n")
f.write("0 1 1\n")
f.close()
print ("finding blokjes van 2bij2")
print ("upper left")
groendicht_lb4 = FocalStatistics(groendicht0, NbrIrregular(temp_dir+"\\LB4.txt"), "SUM", "DATA")
print ("upper right")
groendicht_rb4 = FocalStatistics(groendicht0, NbrIrregular(temp_dir+"\\RB4.txt"), "SUM", "DATA")
print ("lower right")
groendicht_ro4 = FocalStatistics(groendicht0, NbrIrregular(temp_dir+"\\RO4.txt"), "SUM", "DATA")
print ("lower left")
groendicht_lo4 = FocalStatistics(groendicht0, NbrIrregular(temp_dir+"\\LO4.txt"), "SUM", "DATA")

# Voer het Thin-commando uit, nadat alle nullen op NoData zijn gezet. Thin moet ook met de nullen overweg kunnen, maar dat leverde niet
# de gewenste resultaten op.
print ("Thinning lines")
groendicht_nd = SetNull(groendicht0, groendicht0, "VALUE = 0")  # 0 vervangen door NoData
groen_thin0 = Thin (groendicht_nd, "NODATA", "NO_FILTER", "SHARP", "25")  # THIN!
groen_thin = Con(IsNull(groen_thin0), 0, groen_thin0)  # 0 terug op plek van Nodata

# Blokjes van 2bij2 moeten behouden blijven evenals de lijnen die over zijn gebleven na het thinnen. Ook de geisoleerde cellen moeten behouden blijven, alsmede
# voor meer dan de helft gevulde cellen die door het Thin-commando zijn verdwenen.
print ("finding dicht groen")
groendicht4 = ((groendicht_lb4 == 4) | (groendicht_lo4 == 4) | (groendicht_rb4 == 4) | (groendicht_ro4 == 4)) & groendicht0
groendicht = (groendicht4 | groen_thin | ((vlk_gr_d03bij3 == 1) & (groendicht0 == 1)) | (vlkgroen > 312))  
#                                       sum == 1 en zelf == 1 ie geisoleerde dichte cel | ??
# Hier eindigt de Thinning.


# Doe voor het vlakvormige rood hetzelfde als voor het vlakvormige groen en combineer daarna het dichte rood met het dichte groen.
# Vergelijk met groendicht boven!
print ("finding dicht rood")
rooddicht = ((vlk_rd_mx3bij3 > 624) & (vlkrood > 156)) | ((vlk_rd_mx3bij3 <= 624) & (vlkrood > 31))

print ("combining dicht groen en dicht rood")
#Er wordt 1 opgeteld om ervoor te zorgen dat open gebied een 1 krijgt en gesloten gebied een 2, is nodig voor ViewScape
gesloten = (groendicht | rooddicht) + 1  # Hier eventueel groendicht0 gebruiken?
gesloten.save(out_raster_pad+"\\Gesloten_"+year)
arcpy.conversion.RasterToFloat(gesloten, out_flt_pad+"\\Gesloten_"+year+"_"+datelabel+".FLT")

a = datetime.datetime.now().replace(microsecond=0)
print("finished",a)
del arcpy

