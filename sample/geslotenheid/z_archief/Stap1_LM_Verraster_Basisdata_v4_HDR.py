# ---------------------------------------------------------------------------
# Stap1_LM_Verraster_Basisdata_v4.py
# Created on: 2020-05-04
# Description: 
# ---------------------------------------------------------------------------

# Update Hans Roelofsen mei 2021 voor update 2021.

# Import arcpy module
import arcpy
import datetime
from arcpy import env

'''Create timestamp. All output names will be extended with it, so previous output will never be overwritten. '''
t0 = datetime.datetime.now()
date_label = 'time{0}'.format(t0.strftime("%Y%m%d%H%M%S"))
print("started @ {0}".format(t0.strftime("%Y%m%d%H%M%S")))

'''Set Geoprocessing environments and settings'''
env.overwriteOutput = True
env.parallelProcessingFactor = "50%"
arcpy.CheckOutExtension("spatial")
env.scratchWorkspace = r"c:\apps\temp_geodata\scratch.gdb"

'''Set processing extent. Several extents that can be used while testing the script'''
#Netherlands
env.extent = "10000, 300000, 280000, 625000"
#Testing area
#env.extent = "169000, 440000, 174000, 443000,"

'''Set path names'''
temp_pad = r"C:\apps\temp_geodata\scratch.gdb"
temp_features = temp_pad + "\TempFeatures"
out_pad = r"C:\apps\temp_geodata\openheid\LM_BasisRasters2021.gdb"

'''Input 2018'''
#top10_version = "TOP10NL_2018-sept"
#in_bebouwing_version = "Monitoringsbron2018"

'''Input 2019'''
top10_version = "TOP10NL_2019_Sep"
in_bebouwing_version = "Monitoringsbron2019"

'''Input 2020'''
# top10_version = "TOP10NL_2020_sep"

'''Define input paths and dictionaryies'''
top10_pad = r"W:\PROJECTS\GeoDeskData\TOP10NL\{}\TOP10NL.gdb".format(top10_version)
bebouwing_pad = r"W:\PROJECTS\Landschapsmonitor\Openheid\c_viewscape_input\bebouwing\Monitoringsbron.gdb"

# Table with reclass values for blocking land use types. Reclass from In_code --> LM_code
reclass_table = r"W:\PROJECTS\Landschapsmonitor\Openheid\c_viewscape_input\coderingen_en_mask\LM_Coderingen.gdb\LM_Reclass_Top10"

# Field name in reclass_table with values to reclass. Field name in rasters that will be reclassed, depends on source.
in_code_field_name = "In_code"

# Layers_dict is a dictionary with feature classes to process. The value of the key itself will be used in defining the
# rasteriation process and the output names for the layers.
# If the second part of the layer name = 'Lijn', the feature class should be of type Polyline!
# The dictionary values are lists with: path to the feature class, the name of feature class itself and the name
# of the field the reclassification will be based upon.
layers_dict = {"Top10_Terrein": {'in_path': top10_pad, 'fc': "TERREIN_VLAK", 'in_field': "VISUALISATIECODE",
                                 'keys_in_fc': set(), 'all_reclass_keys': set(), 'selected_reclass_keys': set()},
               "Top10_Lijn": {'in_path': top10_pad, 'fc': "INRICHTINGSELEMENT_LIJN", 'in_field': "VISUALISATIECODE",
                              'keys_in_fc': set(), 'all_reclass_keys': set(), 'selected_reclass_keys': set()},
               "LM_Bebouwing": {'in_path': bebouwing_pad, 'fc': in_bebouwing_version, 'in_field': "OBJECT_TYPE",
                                'keys_in_fc': set(), 'all_reclass_keys': set(), 'selected_reclass_keys': set()}}

# Dictionary with actual reclass values. Key = from code, value = to code.
reclass_dict = dict()

'''Read reclass values'''
print('reading reclass values')
cursor = arcpy.SearchCursor(reclass_table)
for srow in cursor:

    # Try to get in_code as integer, else as string
    try:
        in_code = int(srow.getValue(in_code_field_name))
    except ValueError:
        in_code = srow.getValue(in_code_field_name)
    assert isinstance(in_code, (str, int))

    # -1 indicates that corresponding feature doesn't need to be reclassed (from in_code to LM_code) and rasterized
    if srow.LM_code != -1:

        # Add this combination to the reclass dictionary
        reclass_dict[in_code] = srow.LM_code

        # Add to valid values dict
        try:
            layers_dict[srow.F_class]['selected_reclass_keys'].add(in_code)
        except KeyError as e:
            print('unknown fc encountered {0} in Att Field "F_class"'.format(e))

    # all known values in the reclass table are collected to compare them with the actual values in the input feature class
    try:
        layers_dict[srow.F_class]['all_reclass_keys'].add(in_code)
    except KeyError as e:
        print('unknown fc encountered: {}'.format(e))

'''Assert that selected values are a subset of all values'''
for layer in layers_dict.keys():
    assert layers_dict[layer]['selected_reclass_keys'].issubset(layers_dict[layer]['all_reclass_keys']), "error 4 {}".format(layer)

'''Collect all values in the assigned field of each Featureclass'''
for layer in layers_dict.keys():
    in_features = '{0}\{1}'.format(layers_dict[layer]['in_path'], layers_dict[layer]['fc'])
    print("finding unique values for features {}".format(in_features))
    scur = arcpy.SearchCursor(in_features)
    for srow in scur:
        layers_dict[layer]['keys_in_fc'].add(srow.getValue(layers_dict[layer]['in_field']))
    del scur

'''Assert that the values provided in each FC are all mentioned in the reclass table.'''
for layer in layers_dict.keys():
    provided_vals = layers_dict[layer]['keys_in_fc']
    all_vals_in_reclass_tab = layers_dict[layer]['all_reclass_keys']
    if provided_vals.issubset(all_vals_in_reclass_tab):
        print("Congratulations! No values are missing in reclass table for {}".format(layer))
    else:
        diff = provided_vals.difference(all_vals_in_reclass_tab)
        raise AssertionError("fc {0} contains unknown values: {1}".format(layer, ', '.join(list(map(str, diff)))))

for layer, specs in layers_dict.items():

    in_features = '{}\{}'.format(specs['in_path'], specs['fc'])
    valid_vals = list(specs['selected_reclass_keys'])

    # build query expression that will be used to select all valid features and create feature layer.
    if all([isinstance(x, str) for x in valid_vals]):
        query = ' or '.join(["{0} = '{1}'".format(specs['in_field'], x) for x in valid_vals])
    elif all([isinstance(x, int) for x in valid_vals]):
        query = ' or '.join(["{0} = {1}".format(specs['in_field'], x) for x in valid_vals])
    else:
        raise AssertionError('Valid values are of mixed type...')

    print("copying features from {0} w. query: {1}".format(in_features, query))
    arcpy.MakeFeatureLayer_management(in_features, "Featurelayer", query)
    print(arcpy.GetMessages())

    # copy the selected features to a local drive location, for better performance and adding an attribute to the attribute table
    print("{} features selected".format(arcpy.GetCount_management("Featurelayer")[0]))
    arcpy.CopyFeatures_management("Featurelayer", temp_features)
    print(arcpy.GetMessages())

    # add value field to hold the reclass value for each feature
    print("calculating raster value")
    arcpy.AddField_management(temp_features, "Value", "SHORT")
    print(arcpy.GetMessages())

    # For each record, calculate the reclass value.
    ucur = arcpy.UpdateCursor(temp_features)
    for urow in ucur:
        urow.Value = reclass_dict[urow.getValue(specs['in_field'])]
        ucur.updateRow(urow)
    del urow
    del ucur

    '''Finally rasterization can take place'''
    print('Rasterizing')

    # Define the out raster names, depending on the layer name.
    if layer.split("_")[0] == "Top10":
        in_top10_version = top10_version.replace('-', '_')#in case top10_version has a '-' in its name, it needs to be replaced while it's not allowed in a raster name
        out_rastername = layer+"_"+in_top10_version+"_"+date_label
    elif layer.split("_")[0] == "LM":
        out_rastername = layer+"_"+in_bebouwing_version+"_"+date_label

    # Choose the right rasterization process.
    if layer.split("_")[1] == "Lijn":
        arcpy.PolylineToRaster_conversion(temp_features, "Value", out_pad+"\\"+out_rastername, "MAXIMUM_LENGTH", "#", "2.5")
        print(arcpy.GetMessages())
    else:
        arcpy.PolygonToRaster_conversion(temp_features, "Value", out_pad+"\\"+out_rastername, "CELL_CENTER", "#", "2.5")
        print(arcpy.GetMessages())


del arcpy
a = datetime.datetime.now().replace(microsecond=0)
print("Finished:", a)
