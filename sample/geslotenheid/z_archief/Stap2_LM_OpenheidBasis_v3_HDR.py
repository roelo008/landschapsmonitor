# ---------------------------------------------------------------------------
# Stap2_LM_OpenheidBasis_v2.py
# Created on: Mon February 8 2010 10:00:00 AM
# Description: Creates a basemap for ViewScape
# ---------------------------------------------------------------------------

# 21 juli 2010
# Minimale oppervlakte voor vlakvormig groen stond op GT 38. Moest GT 37 zijn.
# 26 maart 2013
# OpenheidBasis.py herschreven voor arcpy en input via inputfile. Output van dit script getest op Viris2009 en geeft
# exact dezelfde resultaten als script OpenheidBasis.py
# 13 maart 2018
# Aangepast voor nieuwe openheidsberekening
# 11 mei 2021
# Overgenomen door Hans Roelofsen met intentie om script leesbaar en sneller te maken, maar inhoudelijk niks veranderen.

# Import arcpy module
import arcpy
import datetime
import os
from arcpy import env

'''Defineer timestamp'''
a = datetime.datetime.now()
datelabel = '{0}time{1}'.format(a.strftime('%Y%m%d'), a.strftime('%H%M%S'))
print("started:", datelabel)

'''Definieer een aantal environment variabelen en directories voor tijdelijke output'''
arcpy.CheckOutExtension("spatial")
env.scratchWorkspace = r"c:\apps\temp_geodata\scratch.gdb"
temp_dir = r"C:\apps\temp_geodata\scratch.gdb"
env.extent = "10000 300000 280000 625000"
mask = r"W:\PROJECTS\Landschapsmonitor\Openheid\c_viewscape_input\coderingen_en_mask\LM_Coderingen.gdb\mask_nl"
env.mask = mask
env.parallelProcessingFactor = "50%"
env.overwriteOutput = True

'''Definieer path voor input'''
in_path = r"C:\apps\temp_geodata\openheid\LM_BasisRasters.gdb"

'''Input voor 2018'''
#year = "2018"
#bebouwing = in_pad+"\\LM_Bebouwing_Monitoringsbron"+year+"_time20200626152231"
#lijnvormig = in_pad+"\\Top10_Lijn_TOP10NL_"+year+"_sept_time20200629091258"
#terrein = in_pad+"\\Top10_Terrein_TOP10NL_"+year+"_sept_time20200629091258"

'''Input voor 2019'''
year = "2019"
bebouwing = "LM_Bebouwing_Monitoringsbron{}_time20200812120927".format(year)
lijnvormig = "Top10_Lijn_TOP10NL_{}_Sep_time20200812120927".format(year)
terrein = "Top10_Terrein_TOP10NL_{}_Sep_time20200812120927".format(year)

'''Input voor 2020
year = 2020
bebouwing = ''
lijnvormig = ''
terrein = ''
'''

'''Definieer locaties voor output'''
out_root = r"c:\apps\temp_geodata\openheid"
newgdb = "Blocking_{0}_{1}.gdb".format(year, datelabel)
arcpy.CreateFileGDB_management(out_folder_path=out_root, out_name=newgdb)
out_raster_path = os.path.join(out_root, newgdb)
out_flt_path = os.path.join(out_root, 'flts')

'''Hier begint het feitelijke rekenwerk aan de drie input-rasrers. Er wordt vanuitgegaan dat alle waardes groter dan nul
 zichtbelemmerende objecten vertegenwoordigen. De 'Raster(naam) > 0' levert een 0 (False) of 1 (True) op. Die worden 
 geaggregeerd met een factor 10 op basis van SUM (default) en vermenigvuldigd met 6,25 als maat voor de oppervlakte in 
 de cel of met 2,5 als maat voor de lengte in de cel.'''

print("Finding linear elements")
lijngroen_nd = 2.5*(arcpy.sa.Aggregate((arcpy.sa.Raster(lijnvormig) > 0), 10))  # Default Aggregation type = SUM!
lijngroen = arcpy.sa.Con(arcpy.sa.IsNull(lijngroen_nd), 0, lijngroen_nd)  # Vervang alle NoData door een 0 (nul).
lijngroen.save(os.path.join(out_raster_path, "Lijngroen"))

print("Finding 'forests'")
vlkgroen_nd = 6.25*(arcpy.sa.Aggregate((arcpy.sa.Raster(terrein) > 0), 10))
vlkgroen = arcpy.sa.Con(arcpy.sa.IsNull(vlkgroen_nd), 0, vlkgroen_nd)  # Vervang alle NoData door een 0 (nul).
vlkgroen.save(os.path.join(out_raster_path, "VlakGroen"))

print("Finding built up area")
vlkrood_nd = 6.25*(arcpy.sa.Aggregate((arcpy.sa.Raster(bebouwing) > 0), 10))
vlkrood = arcpy.sa.Con(arcpy.sa.IsNull(vlkrood_nd), 0, vlkrood_nd)  # Vervang alle NoData door een 0 (nul).
vlkrood.save(os.path.join(out_raster_path, "VlakRood"))

''' Voor elke cel wordt bepaald hoeveel 'groen' of 'rood' er maximaal wordt aangetroffen in een omgeving van drie bij 
drie cellen. Dit is nodig om cellen grenzend aan volledig gevulde cellen (bijvoorbeeld cellen aan een bosrand) anders 
te behandelen dan cellen in 'het open veld'.'''
print("calculating focal max vlakvormig groen")
vlk_gr_mx3bij3 = arcpy.sa.FocalStatistics(vlkgroen, arcpy.sa.NbrRectangle(3, 3, "CELL"), "MAXIMUM", "DATA")
print("calculating focal max vlakvormig rood")
vlk_rd_mx3bij3 = arcpy.sa.FocalStatistics(vlkrood, arcpy.sa.NbrRectangle(3, 3, "CELL"), "MAXIMUM", "DATA")

'''Alle cellen die grenzen aan een volle cel (> 624) moeten voor meer dan de helft gevuld zijn (> 312), om zo weinig 
mogelijk van de open ruimte 'af te snoepen'. Voor cellen die niet grenzen aan een volle cel ligt het criterium lager. 
Daarin moet minimaal 37 vierkante meter vlakvormig groen zitten (minimale breedte van 3 meter x helft van de doorsnede
van een cel). Lijnvormige elementen moeten minimaal de helft van de doorsnede van een cel vullen. Daarbij is naar 
beneden afgerond (12 ipv 13) omdat dat op een aantal plaatsen een beter resultaat gaf.'''
print("finding potentieel dicht groen")
groendicht0 = (((vlk_gr_mx3bij3 > 624) & (vlkgroen > 312)) | ((vlk_gr_mx3bij3 <= 624) & (vlkgroen > 37)) | (lijngroen >= 12))

'''De geisoleerde groene cellen moeten worden opgespoord om ze later terug te kunnen zetten. Ze verdwijnen namelijk na 
het Thin commando verderop. Hiervoor is een FocalStatistics nodig. Is de cel zelf 'groen' en heeft de som van drie bij 
drie cellencellen de waarde 1, dan gaat het om een geisoleerde cel.'''
print("calculating focal sum potentieel groen")
vlk_gr_d03bij3 = arcpy.sa.FocalStatistics(groendicht0, arcpy.sa.NbrRectangle(3,3,"CELL"), "SUM", "DATA")

'''Diagonale lijnen doorsnijden naast elkaar gelegen cellen waardoor ze meer cellen uit de open ruimte opslokken dan 
noodzakelijk is. Dit kan worden opgelost met het commando Thin. Echter, op plaatsen waar verschillende lijnen bij elkaar
 komen ontstaan daarbij ongewenste effecten als afrondingen. Om dit te voorkomen worden cellen opgezocht die onderdeel 
 zijn van een blokje van 2 bij 2 cellen en voor die cellen wordt het Tin-commando ongedaan gemaakt.'''

''' Ga op zoek naar de cellen die onderdeel zijn van een blokje van 2 bij 2 cellen. Schrijf eerst de vier benodigde 
kernel files voor vier cellen, resp. linksboven, rechtsboven, linksonder en rechtsonder in een omgeving van drie bij 
drie cellen en voer daarna de FocalStatistics uit.'''
LB = os.path.join(temp_dir, "LB4.txt")
RB = os.path.join(temp_dir, "RB4.txt")
RO = os.path.join(temp_dir, "RO4.txt")
LO = os.path.join(temp_dir, "LO4.txt")
with open(LB, 'w') as f:
    f.write("3 3\n1 1 0\n1 1 0\n0 0 0\n")
with open(RB, 'w') as f:
    f.write("3 3\n0 1 1\n0 1 1\n0 0 0\n")
with open(RO, 'w') as f:
    f.write("3 3\n0 0 0\n0 1 1\n0 1 1\n")
with open(LO, 'w') as f:
    f.write("3 3\n0 0 0\n1 1 0\n1 1 0\n")

print("finding blokjes van 2bij2")
print("upper left")
groendicht_lb4 = arcpy.sa.FocalStatistics(groendicht0, arcpy.sa.NbrIrregular(LB), "SUM", "DATA")
print("upper right")
groendicht_rb4 = arcpy.sa.FocalStatistics(groendicht0, arcpy.sa.NbrIrregular(RB), "SUM", "DATA")
print("lower right")
groendicht_ro4 = arcpy.sa.FocalStatistics(groendicht0, arcpy.sa.NbrIrregular(RO), "SUM", "DATA")
print("lower left")
groendicht_lo4 = arcpy.sa.FocalStatistics(groendicht0, arcpy.sa.NbrIrregular(LO), "SUM", "DATA")

# Voer het Thin-commando uit, nadat alle nullen op NoData zijn gezet. Thin moet ook met de nullen overweg kunnen,
# maar dat leverde niet de gewenste resultaten op.
print("Thinning lines")
groendicht_nd = arcpy.sa.SetNull(groendicht0, groendicht0, "VALUE = 0")
groen_thin0 = arcpy.sa.Thin(groendicht_nd, "NODATA", "NO_FILTER", "SHARP", "25")
groen_thin = arcpy.sa.Con(arcpy.sa.IsNull(groen_thin0), 0, groen_thin0)

# Blokjes van 2bij2 moeten behouden blijven evenals de lijnen die over zijn gebleven na het thinnen. Ook de geisoleerde
# cellen moeten behouden blijven, alsmede voor meer dan de helft gevulde cellen die door het Thin-commando zijn
# verdwenen.
print("finding dicht groen")
groendicht4 = ((groendicht_lb4 == 4) | (groendicht_lo4 == 4) | (groendicht_rb4 == 4) | (groendicht_ro4 == 4)) & groendicht0
groendicht = (groendicht4 | groen_thin | ((vlk_gr_d03bij3 == 1) & (groendicht0 == 1)) | (vlkgroen > 312))  

# Doe voor het vlakvormige rood hetzelfde als voor het vlakvormige groen en combineer daarna het dichte rood met
# het dichte groen.
print("finding dicht rood")
rooddicht = ((vlk_rd_mx3bij3 > 624) & (vlkrood > 156)) | ((vlk_rd_mx3bij3 <= 624) & (vlkrood > 31))
print("combining dicht groen en dicht rood")

#Er wordt 1 opgeteld om ervoor te zorgen dat open gebied een 1 krijgt en gesloten gebied een 2, is nodig voor ViewScape
gesloten = (groendicht | rooddicht) + 1
gesloten.save(out_raster_path + "\\Gesloten_" + year)
arcpy.conversion.RasterToFloat(gesloten, out_flt_path + "\\Gesloten_" + year + "_" + datelabel + ".FLT")

a = datetime.datetime.now().replace(microsecond=0)
print("finished",a)
del arcpy

