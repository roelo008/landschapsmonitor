import os
import datetime

def readme(outfile, top10_gdb, bebouwing_gdb, bebouwing_fc, opgaand_groen_gdb,
           th_vlak, th_lijn, th_combi, bbsel, t10vlaksel, t10lijnsel):

    dirname = os.path.dirname(outfile)
    name, ext = os.path.splitext(os.path.basename(outfile))

    with open('{}/{}_readme.txt'.format(dirname, name), 'w') as f:

        f.write('Metadata for {}\n'.format(outfile))
        f.write('Created on: {}\n'.format(datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')))
        f.write('Created by: {}\n'.format(os.environ.get('USERNAME')))
        f.write('Source Data:\n"')
        f.write("  top10 Vlak: {}\TERREIN_VLAK\n".format(top10_gdb))
        f.write("  top10 Lijn: {}\INRICHTINGSELEMENT_LIJN\n".format(top10_gdb))
        f.write("  bebouwingsindicator: {}\{}\n".format(bebouwing_gdb, bebouwing_fc))
        f.write("  opgaand groen vlak: {}\Vlakken\n".format(opgaand_groen_gdb))
        f.write("  opgaand groen lijn: {}\Lijnen\n".format(opgaand_groen_gdb))
        f.write("drempelwaarde vlakken: {}\n".format(th_vlak))
        f.write("drempelwaarde lijnen: {}\n".format(th_lijn))
        f.write("drempelwaarde beiden: {}\n  ".format(th_combi))
        f.write('selectie uit bebouwingsindicator: {}\n'.format(bbsel))
        f.write('selectie uit Top10 Vlak: {}\n'.format(t10vlaksel))
        f.write('selectie uit Top10 lijn: {}\n'.format(t10lijnsel))

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('of', help='outfile')
    parser.add_argument('t10gdb', help='top10 fgdb')
    parser.add_argument('bbgdb', help='bebouwing fgdb')
    parser.add_argument('bbfc', help='bebouwing featureclass')
    parser.add_argument('oggdb', help='opgaand groen fgdb')
    parser.add_argument('thv', help='threshold vlakvormig')
    parser.add_argument('thl', help='threshold lijnvormig')
    parser.add_argument('thc', help='threshold combi')
    parser.add_argument('bbsel', help='selected items from bebouwingsindicator')
    parser.add_argument('t10vlaksel', help='selected items from Top10 Vlak')
    parser.add_argument('t10lijnsel', help='selected items from Top10 Lijn')

    args = parser.parse_args()

    readme(args.of, args.t10gdb, args.bbgdb, args.bbfc, args.oggdb, args.thv, args.thl, args.thc,
           args.bbsel, args.t10vlaksel, args.t10lijnsel)





