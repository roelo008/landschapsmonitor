REM Batch script for creating Opgaande Elementen kaart as input for Viewscape. Werkt momenteel enkel voor jaargangen 2018 en 2019.
REM Project: Landschapsmonitor 
REM Door: Hans Roelofsen, WEnR, team B&B

setlocal enabledelayedexpansion

:: Data Schema
:: Peil Datum 	Top10		Bebouwing		OpgaandGroen
:: 31-12-2015   november 2015	<VOLGT>         	2016
:: 31-12-2017   november 2017	Monitoringsbron_2018    2018
:: 31-12-2018 	november 2018	Monitoringsbron_2019	2019
:: 31-12-20210	september 2020	Monitoringsbron_2021	2021 

SET jaar=2022
SET peildatum=31-12-2022

REM Opgaand Groen Directories
SET OGDir="w:\projECTS\Landschapsmonitor\cIndicatoren\OpgaandGroen\a_Geodata\leveringPPK20230320\monitoringsbron_opgaandgroen_2022_V01.gdb"

REM Bebouwingsdirectories
SET BBDir="w:\projects\Landschapsmonitor\cIndicatoren\Bebouwing\geodata\leveringPPK_20230323\monitoringsbron_bebouwing_2022_V01.gdb"
SET BBfc=monitoringsbron_bebouwing_2023_V01

REM Top10 Directorie
SET T10Dir=w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\Top10NL_Kadaster\nov2022\TOP10NL.gdb

REM MAsk dir
SET maskdir=w:\PROJECTS\Landschapsmonitor\cIndicatoren\Openheid\c_viewscape_input\coderingen_en_mask\BBGMask

SET scratch_dir=c:\apps\temp_geodata\openheid\scratch\31_12_2022
SET out_dir=c:\apps\temp_geodata\openheid\output
SET gdal_dir=c:\Program Files\QGIS 3.28.4\apps\Python39\Scripts

if not exist %scratch_dir%\NUL md %scratch_dir%
if not exist %out_dir%\NUL md %out_dir%

REM minimum aantal 2.5m cellen in een 25m cel voor kwalificatie als gesloten. Max is natuurlijk 100
SET drempelwaarde_vlakken=6
SET drempelwaarde_lijnen=2
SET drempelwaarde_combi=6


REM Selectie van categorieen uit Bebouwingsindicator, Top10 Vlak en Top10 lijn
SET BB_sel='bunker', 'kas, warenhuis', 'BAG-pand', 'open loods', 'opslagtank', 'windmolen', 'windmolen: korenmolen', 'windmolen: watermolen'
SET T10Vlak_sel='fruitkwekerij', 'boomgaard', 'boomkwekerij', 'dodenakker met bos'
SET T10Lijn_sel='muur', 'geluidswering'

call c:\Users\roelo008\AppData\Local\miniconda3\Scripts\activate.bat
call conda activate vs

REM Use python script get proper timestamp
python create_timestamp.py
set /p timestamp= < tmpFile
del tmpFile

REM RASTERIZE Opgaand Groen
REM gdal_rasterize --config OGR_ORGANIZE_POLYGONS SKIP -at -l Vlakken -of GTiff -te 10000 300000 280000 625000 -tr 2.5 2.5 -ot Byte -a_srs epsg:28992 -co COMPRESS=LZW -burn 1 %OGDir% %scratch_dir%/opgaandgroen_vlak.tif
REM gdal_rasterize --config OGR_ORGANIZE_POLYGONS SKIP -at -l Lijnen -of GTiff -te 10000 300000 280000 625000 -tr 2.5 2.5 -ot Byte -a_srs epsg:28992 -co COMPRESS=LZW -burn 1 %OGDir% %scratch_dir%/opgaandgroen_lijn.tif

REM RASTERIZE Bebouwing
REM gdal_rasterize --config OGR_ORGANIZE_POLYGONS SKIP -at -l %BBfc% -where "OBJECT_TYPE IN (%BB_sel%)" -of GTiff -te 10000 300000 280000 625000 -tr 2.5 2.5 -ts 108000 130000 -ot Byte -a_srs epsg:28992 -co COMPRESS=LZW -burn 1 %BBDir% %scratch_dir%/bebouwing_sel.tif
 
REM RASTERIZE Top10 Vlak
REM gdal_rasterize --config OGR_ORGANIZE_POLYGONS SKIP -where "TYPELANDGEBRUIK IN (%T10Vlak_sel%)" -at -of GTiff -te 10000 300000 280000 625000 -tr 2.5 2.5 -ts 108000 130000 -ot Byte -a_srs epsg:28992 -l TERREIN_VLAK -co COMPRESS=LZW -burn 1 %T10Dir% %scratch_dir%/t10vlak_sel.tif  

REM RASTERIZE Top10 Lijn
REM gdal_rasterize --config OGR_ORGANIZE_POLYGONS SKIP -where "TYPEINRICHTINGSELEMENT IN (%T10Lijn_sel%)" -at -of GTiff -te 10000 300000 280000 625000 -tr 2.5 2.5 -ts 108000 130000 -ot Byte -a_srs epsg:28992 -l INRICHTINGSELEMENT_LIJN -co COMPRESS=LZW -burn 1 %T10Dir% %scratch_dir%/t10lijn_sel.tif

REM COMBINE Add vlakvormige bronrasters together 
REM python "%gdal_dir%/gdal_calc.py" --quiet --co COMPRESS=LZW -A %scratch_dir%/opgaandgroen_vlak.tif -B %scratch_dir%/bebouwing_sel.tif -C %scratch_dir%/t10vlak_sel.tif --outfile=%scratch_dir%/vlakken_combi.tif --calc="numpy.where((A+B+C)>=1, 1, 0)"

REM COMBINE Add lijnvormige bronrasters together 
REM python "%gdal_dir%/gdal_calc.py" --quiet --co COMPRESS=LZW -A %scratch_dir%/opgaandgroen_lijn.tif -B %scratch_dir%/t10lijn_sel.tif --outfile=%scratch_dir%/lijnen_combi.tif --calc="numpy.where((A+B)>=1, 1, 0)"

REM AGGREGATE lijnen and vlakken to 25m with SUM aggregation method
REM gdalwarp -q -co COMPRESS=LZW -tr 25 25 -te 10000 300000 280000 625000 -r sum %scratch_dir%/vlakken_combi.tif %scratch_dir%/vlakken_combi_25m.tif 
REM gdalwarp -co COMPRESS=LZW -tr 25 25 -te 10000 300000 280000 625000 -r sum %scratch_dir%/lijnen_combi.tif %scratch_dir%/lijnen_combi_25m.tif 

REM Combineer lijnen en vlakken om gebieden op te sporen waar lijnen of vlakken individueel niet voldoende zijn om de drempelwaarde(s) te overschrijden
python "%gdal_dir%/gdal_calc.py"  --co COMPRESS=LZW -A %scratch_dir%/vlakken_combi_25m.tif -B %scratch_dir%/lijnen_combi_25m.tif --outfile=%scratch_dir%/lijn_vlak_combi_25m.tif --calc="A+B"

REM Apply treshold
python "%gdal_dir%/gdal_calc.py" --co COMPRESS=LZW -A %scratch_dir%/vlakken_combi_25m.tif --outfile=%scratch_dir%/vlakken_gte_th.tif --calc="numpy.where(A >= %drempelwaarde_vlakken%, 1, 0)"
python "%gdal_dir%/gdal_calc.py" --co COMPRESS=LZW -A %scratch_dir%/lijnen_combi_25m.tif --outfile=%scratch_dir%/lijnen_gte_th.tif --calc="numpy.where(A >= %drempelwaarde_lijnen%, 1, 0)"
python "%gdal_dir%/gdal_calc.py" --co COMPRESS=LZW -A %scratch_dir%/lijn_vlak_combi_25m.tif --outfile=%scratch_dir%/lijnen_vlakken_gte_th.tif --calc="numpy.where(A >= %drempelwaarde_combi%, 1, 0)"

REM Combine. Output pxl value 2 means 'gesloten' 1 means 'open'.
python "%gdal_dir%/gdal_calc.py" --co COMPRESS=LZW --NoDataValue=-9999 --type=Float32 -A %scratch_dir%/vlakken_gte_th.tif -B %scratch_dir%/lijnen_gte_th.tif -C %scratch_dir%/lijnen_vlakken_gte_th.tif -D %maskdir%\BBGExtent_25m.tif --outfile=%out_dir%\opgaande_elementen_%jaar%_%timestamp%.tif --calc="numpy.where(D == 1, numpy.where((A+B+C) >=1, 2, 1), -9999)"

REM add metadata to output file
python "%gdal_dir%/gdal_edit.py" -mo CREATEDBY=%USERNAME% -mo DATETIME=%timestamp% -mo TOP10SRC=%T10Dir% -mo BEBOUWINGSRC=%BBDIR%\%BBfc% -mo OGSRC=%OGDIR% -mo THvlak=%drempelwaarde_vlakken% -mo THlijn=%drempelwaarde_lijnen% -mo THcombi=%drempelwaarde_combi% %out_dir%\opgaande_elementen_%jaar%_%timestamp%.tif

REM Translate to flt
gdal_translate -ot Float32 -of "EHdr" %out_dir%\opgaande_elementen_%jaar%_%timestamp%.tif %out_dir%\opgaande_elementen_%jaar%_%timestamp%.flt

REM remove old header file
ren %out_dir%\opgaande_elementen_%jaar%_%timestamp%.hdr opgaande_elementen_%jaar%_%timestamp%_OLD.hdr

REM create proper header file
python replace_hdr.py %out_dir% opgaande_elementen_%jaar%_%timestamp%.hdr

REM Write metadata file
python create_readme.py %out_dir%\opgaande_elementen_%jaar%_%timestamp%.tif %T10Dir% %BBDir% %BBfc% %OGDir% %drempelwaarde_vlakken% %drempelwaarde_lijnen% %drempelwaarde_combi% "%BB_sel%" "%T10Vlak_sel%" "%T10Lijn_sel%"

pause
