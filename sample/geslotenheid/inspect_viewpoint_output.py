import sys
import os
import geopandas as gp
from shapely.geometry import Point
import pandas as pd

# VIEWPOINTS_FILE = r'C:/apps/temp_geodata/windturbiens/azure/DataFiles/flevoland_pnts_200m_sel.shp'
# RESULTS_FILE = r'c:\apps\temp_geodata\windturbiens\testsHans\output\rawtable_results_test01_hdr.csv'
# VIEWPOINTS = [4]
# OUT_DIR = r''


def viewscape_output_inspection(results: str,
                                viewpoints: int,
                                viewpoints_file: str,
                                out_dir: str):
    """
    Select ViewScape output associated with one or more viewpoints and write to file

    Parameters
    ----------
    results
    viewpoints
    viewpoints_file
    out_dir

    Returns
    -------

    """

    viewpoint_string = ", ".join([str(i) for i in viewpoints])

    if results.endswith('exp'):
        vp_data = pd.read_csv()

    vp_data = pd.read_csv(results).query(f"VP_ID in [{viewpoint_string}]")
    if vp_data.empty:
        print(f'No data found for viewpoint {viewpoint_string}')
        sys.exit(1)


    out_name = f"viewscape_inspection_vp{viewpoint_string.replace(', ', '_')}"
    gp.GeoDataFrame(data=vp_data.drop(columns=["x", "y"]),
                          geometry=[Point(x, y) for x,y in zip(vp_data.x, vp_data.y)]).set_crs(epsg=28992).to_file(os.path.join(
        out_dir, f'{out_name}_windturbines.shp'
    ))

    gp.read_file(viewpoints_file).query(f"ID in [{viewpoint_string}]").to_file(
        os.path.join(out_dir, f'{out_name}_viewpoints.shp')
    )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('results', type=str, help='Viewscape output file')
    parser.add_argument('viewpoints', type=int, nargs='+', help='One or more viewpoint ids')
    parser.add_argument('viewpoints_file', type=str, help='Shapefile with viewpoints')
    parser.add_argument('out_dir', help='output directory', type=str)
    args = parser.parse_args()

    viewscape_output_inspection(**vars(args))


