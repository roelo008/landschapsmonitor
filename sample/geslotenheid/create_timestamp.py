import datetime

"""Create timestamp and write to file. Hans Roelofsen, WENR, 8/Dec/2021 12:35"""

timestamp = datetime.datetime.now().strftime('%Y%m%d_%H%M')
with open(r'tmpFile', 'w') as f:
    f.write(timestamp)