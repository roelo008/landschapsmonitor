REM call c:\Users\roelo008\Miniconda3\Scripts\activate.bat
REM call conda activate mrt

REM Use python script get proper timestamp
python ../create_timestamp.py
set /p timestamp= < tmpFile
del tmpFile

echo %timestamp%

REM CMD Line arguments: 1: data path. 2: year

SET gdal_dir=c:\Program Files\QGIS 3.28.4\apps\Python39\Scripts
SET start_dir=%CD%
SET data_dir=%1%
SET zip_dir=1zip
SET shps_dir=2shps
SET shp_dir=3shp
SET tif_dir=4tif
SET naambasis=monitoringsbron_openheid

REM Merge shapefiles into single shapefile
python "%gdal_dir%"\ogrmerge.py -o %data_dir%/%shp_dir%/%naambasis%_%timestamp%_merged.shp %data_dir%/%shps_dir%/*.shp -single

REM Rasterize
gdal_rasterize -co COMPRESS=LZW -a_nodata 0 -a_srs epsg:28992 -a T_VISAREA -l %naambasis%_%timestamp%_merged -te 10000 300000 280000 625000 -tr 100 100 %data_dir%\%shp_dir%\%naambasis%_%timestamp%_merged.shp %data_dir%\%tif_dir%\%naambasis%_%timestamp%_raw.tif

REM Calculate to hectares
python "%gdal_dir%"/gdal_calc.py -A %data_dir%\%tif_dir%\%naambasis%_%timestamp%_raw.tif --co COMPRESS=LZW --outfile=%data_dir%\%tif_dir%/%naambasis%_%2%.tif --calc="A/10000"

REM Add metadata
python "%gdal_dir%/gdal_edit.py" -mo CREATED_BY=%USERNAME% -mo CREATIONDATE=%timestamp% -mo CONTENT=openheid_in_hectare -mo COPYRIGHT=WEnR2021 -mo ORIGIN=VIEWSCAPE -mo PROJECT=Lanschapsmonitor %data_dir%\%tif_dir%\%naambasis%_%2%.tif

REM copy template layer file and rename
Copy w:\PROJECTS\Landschapsmonitor\cIndicatoren\Openheid\b_layers\Openheidsklassn.qml %data_dir%\%tif_dir%
W:
cd %data_dir%\%tif_dir%
rename Openheidsklassn.qml %naambasis%_%2%.qml
C:
cd %start_dir%
pause