"""
Creatie van CLO cijfers en figuren voor openheid
Toestand: voor jaar X, PER PROVINCIE, aantal obs-punten met openheid klasses/totaal aantal punten per provincie
  < 1, 1-10, 10-50, 50-100, 100-300, 300-750, 750-1500, >1500


Workflow TOESTAND
1. samengevoegde punten shapefile van Viewscape inlezen en elk punt koppelen aan een provincie!
2. elk punt klassificieren in een openheidsklasse
3. aantal punten per klasse per provincie berekenen
4. totaal aantal punten per provincie
5. rapporteren

Verschil: Viewscape shapefile voor 2 jaren inlezen. Punten toekennen aan veschilklasse:
veel geslotener, aanzienlijk gesloten, beperkt gesloten, gelijk, beperkt open, aanzienlijk openenr, veel opener
Aantal punten per klasse per province
etc.


"""

import os
import numpy as np
import pandas as pd
import geopandas as gp
import datetime

def classify(x, arr, class_names):
    """
    Classify x into an array
    :param x:
    :param arr: array with class boundaries
    :param class_names: class names
    :return: class name
    """
    if np.isnan(x):
        return np.nan
    else:
        return class_names[np.digitize(x, arr)]

# Years
yearA = 2019
yearB = 2021

# Compare merged viewscape horizon outputs for 2 years: yearA, yearB. yearA is assumed to be < year B
pntsA_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Openheid\d_viewscape_output\20211011_pd01012019\3shp\horizon_pd01012019_merged.shp'
pntsB_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Openheid\d_viewscape_output\20211018_pd01012021\3shp\horizon_pd01012021_merged.shp'
provs_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\provincies\incl_water\provincies_2018.shp'
prov_naam = 'provincien'

ts = datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')
ts_short = datetime.datetime.now().strftime('%Y%m%d%H%M%S')

out_dir = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Openheid\h_compendium_teksten'
excel_out = r'IndicatorOpenheidCompendiumSamenvatting_{0}_{1}_{2}.xlsx'.format(ts_short, yearA, yearB)

pntsA = gp.read_file(pntsA_src)
pntsB = gp.read_file(pntsB_src)
provs = gp.read_file(provs_src)

# Merge points together in a single dataframe
pnts = gp.GeoDataFrame(pd.merge(left=pntsA.loc[:, ['VP_ID', 'T_VISAREA']], right=pntsB.loc[:, ['VP_ID', 'T_VISAREA']],
                       left_on='VP_ID', right_on='VP_ID', suffixes=('_{}'.format(yearA), '_{}'.format(yearB))), geometry=pntsA.geometry)

# Couple each point to a provincie
pnts = gp.sjoin(pnts, provs.loc[:, [prov_naam, 'geometry']], how='left')

# Drop grid cells zonder Provincie and remove hokken coupled to > 1 provincie
pnts.dropna(subset=[prov_naam], inplace=True)
pnts.drop_duplicates(subset=['VP_ID'], inplace=True)

# Area in m2 to hectares
pnts['T_VISAREA_{}'.format(yearA)] = pnts['T_VISAREA_{}'.format(yearA)].divide(10000)
pnts['T_VISAREA_{}'.format(yearB)] = pnts['T_VISAREA_{}'.format(yearB)].divide(10000)

# Calculate difference
pnts['diff_ha'] = pnts['T_VISAREA_{}'.format(yearB)].subtract(pnts['T_VISAREA_{}'.format(yearA)])

# Slot areas into categories
vis_area_bins = np.array([1, 10, 50, 100, 300, 750, 1500])
vis_area_classes = ['< 1 ha', '1-10 ha', '10-50 ha', '50-100 ha', '100-300 ha', '300-750 ha', '750-1500 ha', '> 1500 ha']
pnts['class{}'.format(yearA)] = pnts['T_VISAREA_{}'.format(yearA)].apply(classify, arr=vis_area_bins, class_names=vis_area_classes)
pnts['class{}'.format(yearB)] = pnts['T_VISAREA_{}'.format(yearB)].apply(classify, arr=vis_area_bins, class_names=vis_area_classes)

# Slot differences into categories
diff_bins = np.array([-100, -50, -25, 25, 50, 100])
diff_classes = ['Veel geslotener (< -100 ha)', 'Aanzienlijk geslotener (-50 - -100 ha)', 'Beperkt geslotener (-25 - -50 ha)',
                'Geen of nauwelijks verandering (-25 - 25 ha)', 'Beperkt opener (25 - 50 ha)', 'Aanzienlijk opener (50 - 100 ha)',
                'Veel opener (>= 100 ha)']
pnts['diff_class'] = pnts.diff_ha.apply(classify, arr=diff_bins, class_names=diff_classes)

# Pivot per provincie. Units are count of 500m hokken
statsA = pd.pivot_table(data=pnts, index='provincien', columns='class{}'.format(yearA), values='VP_ID', aggfunc='count')
statsB = pd.pivot_table(data=pnts, index='provincien', columns='class{}'.format(yearB), values='VP_ID', aggfunc='count')
statsdiff = pd.pivot_table(data=pnts, index='provincien', columns='diff_class', values='VP_ID', aggfunc='count')

# Write to output excel
with pd.ExcelWriter(os.path.join(out_dir, excel_out), mode='w') as writer:

    metadata = {'Viescape Horizon points {}'.format(yearA): pntsA_src,
                'Viescape Horizon points {}'.format(yearB): pntsB_src,
                'provincie source': provs_src,
                'made by': os.environ.get('USERNAME'),
                'made when': ts,
                'made with': r'https://git.wur.nl/roelo008/landschapsmonitor/-/blob/master/sample/geslotenheid/postprocessing/openheid2indictorv2.py',
                'NOTE': 'Getallen zijn het aantal Viewscape waarnemingspunten.'}
    pd.DataFrame(data=metadata, index=[0]).T.to_excel(writer, sheet_name='Metadata', index=True)
    statsA.loc[:, vis_area_classes].to_excel(writer, sheet_name='stats{}'.format(yearA), index=True)
    statsB.loc[:, vis_area_classes].to_excel(writer, sheet_name='stats{}'.format(yearB), index=True)
    statsdiff.loc[:, diff_classes].to_excel(writer, sheet_name='stats_diff', index=True)
