"""
Postprocessing script to prepare Landgebruik indicator to CLO Indicator format
Mainly:
1. simplify LGN klasses
2. calculate klasse areaal in ha per provincie
3. reporting
Hans Roelofsen, 23 september 2021
"""

import datetime
import rasterio as rio
import rasterstats
import pandas as pd
import numpy as np
import geopandas as gp
import os

def pxls2ha(count, res=5):
    """
    function to apply
    :param count: pxl count
    :param res: pxl resolution in m
    :return: area in ha
    """
    return np.divide(np.multiply(count, np.square(res)), 10000)

out_dir = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Landgebruik\h_compendium_teksten'
excel_out = 'Landgebruik2019_samenvatting_per_provincie.xlsx'

lgn_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Landgebruik\e_indicator\Landgebruik_2019\Landgebruik_2019_14klassen.tif'
legend_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Landgebruik\e_indicator\Landgebruik_2019\Landgebruik_2019_14klassen.tif.vat.dbf'
prov_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\provincies\incl_water\provincies_2018.shp'

# Provincie shapefile
provs = gp.read_file(prov_src)

# LGN raster
lgn = rio.open(lgn_src)

# Originele 14 klasse legenda
legend = gp.read_file(legend_src)
val2lgn14kl = dict(zip(legend.Value, legend.LGN_14klas))

# Versimpelingslegenda tbv CLOR
# TODO

# Areaal per provincie
stats = pd.DataFrame(rasterstats.zonal_stats(vectors=provs, raster=lgn.read(1), affine=lgn.transform,
                                             nodata=lgn.nodata, categorical=True, category_map=val2lgn14kl),
                     index=provs.provincien)
stats_ha = stats.multiply(pxls2ha(1))
stats_perc = stats.divide(stats.sum(axis=1), axis=0).multiply(100)

with pd.ExcelWriter(os.path.join(out_dir, excel_out), mode='w') as writer:

    metadata = {'source image': lgn_src,
                'provincie source': prov_src,
                'legenda source': legend_src,
                'made by': os.environ.get('USERNAME'),
                'made when': datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')}
    pd.DataFrame(data=metadata, index=[0]).T.to_excel(writer, sheet_name='Metadata', index=True)

    stats_ha.to_excel(writer, sheet_name='Statistiek_hectare', index=True)
    stats_perc.to_excel(writer, sheet_name='Statistiek_precentage', index=True)








