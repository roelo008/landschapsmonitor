"""
Postprocessing of LGN 2018-2019 raster file.
Hans Roelofsen, 21-12-2020
"""


import geopandas as gp
import pandas as pd
import numpy as np

df = gp.read_file(r'\\wur\dfs-root\PROJECTS\Landschapsmonitor\Landgebruik\OneDrive_1_12-10-2020\LGN2018-LGN2019_5m_eliminated\bewerkt_HR\LGN2018-LGN2019_5m_eliminated.tif.vat.dbf')
lgn = pd.read_csv(r'\\wur\dfs-root\PROJECTS\Landschapsmonitor\Landgebruik\OneDrive_1_12-10-2020\LGN2018-LGN2019_5m_eliminated\bewerkt_HR\lgn_coderingen.csv')

code2desc = dict(zip(lgn.lgn_code, lgn.lgn_desc))
lgn_code2monitor_code = dict(zip(lgn.lgn_code, lgn.monitor_code))
lgn_code2monitor_desc = dict(zip(lgn.lgn_code, lgn.monitor_desc))
lgn_code2val_code = dict(zip(lgn.lgn_code, lgn.val_code))

df['lgndesc18'] = df.lgn18.map(code2desc)
df['lgndesc19'] = df.lgn19.map(code2desc)

df['moncode18'] = df.lgn18.map(lgn_code2monitor_code)
df['moncode19'] = df.lgn19.map(lgn_code2monitor_code)

df['mondesc18'] = df.lgn18.map(lgn_code2monitor_desc)
df['mondesc19'] = df.lgn19.map(lgn_code2monitor_desc)

df['valcode18'] = df.lgn18.map(lgn_code2val_code)
df['valcode19'] = df.lgn19.map(lgn_code2val_code)

df['mon1819dif'] = np.where(df.moncode18 == df.moncode19, 0, 1)
df['val1819dif'] = np.where(df.valcode18 == df.valcode19, 0, 1)

