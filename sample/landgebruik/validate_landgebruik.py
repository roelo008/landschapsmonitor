"""
Tool to validate landgebruik monitor data based on user-assigned labelling of ~1000 points
Hans Roelofsen, 01 june 2021
"""

import geopandas as gp
import rasterio as rio
import rasterstats as rast
import os


def rastervals_contains(rastervals, validationval, year):
    """
    check if any in a list of raster values contains a validation code at a position identified with year
    :param rasterval: list of values from the LGN source formatted 1819<code_2018><code_2019>
                      where codes are 3 digits
    :param validationval: human assigned, 3 digit land use code
    :param year: look in which part of the rasterval?
    :return: Boolean
    """

    d = {2018: [str(x)[4:7] for x in rastervals],
         2019: [str(x)[7:] for x in rastervals]}
    return True if validationval in d[year] else False

"""Shapefile met 1.000 validation points met waargenomen klasses in 2018 en 2019"""
gdb = r'w:\PROJECTS\Landschapsmonitor\Landgebruik\g_validatie\LandgebruikValidatie_SHP'
fc = 'randompoints_validation_total_LGN.shp'
pnts = gp.read_file(os.path.join(gdb, fc))

"""LGN Tiff met gemodelleerde klasses in 2018 en 2019 gecodeerd in pixelwaardes"""
lgn_src = r'w:\PROJECTS\Landschapsmonitor\Landgebruik\g_validatie\change_ID_lgn_2018_2019_step7v4_val.tif'
lgn = rio.open(lgn_src)

""""Sample LGN for klasses in 25m buffer around validation points"""
lgn_sample = rast.zonal_stats(vectors=pnts.buffer(25), raster=lgn_src, categorical=True, all_touched=True)

"""Reduce to keys only (ie pixel value presence, we are not interested in counts)"""
rastervals_v1 = [list(x.keys()) for x in lgn_sample]  # list is integer lists
rastervals_v2 = [[str(x) for x in y] for y in rastervals_v1]  # list of string lists
rastervals_v3 = list(map(', '.join, rastervals_v2))  # list of strings

pnts['rasterval_buff'] = rastervals_v1
pnts['ras_buff'] = rastervals_v3

"""Query lgn_sample for presence of 2018 validation code"""
pnts['inbuff18'] = pnts.apply(lambda row: rastervals_contains(row.rasterval_buff, row.value2018, 2018), axis=1)
pnts['inbuff19'] = pnts.apply(lambda row: rastervals_contains(row.rasterval_buff, row.value2019, 2019), axis=1)
pnts['inbuffboth'] = pnts.inbuff18 & pnts.inbuff19
pnts['inbuffonce'] = pnts.inbuff18 | pnts.inbuff19

out_dir = r'w:\PROJECTS\Landschapsmonitor\Landgebruik\g_validatie\LandgebruikVa' \
          r'lidatie_SHP'
out_name = 'randompoints_validation_total_LGN_HDR20210603.shp'
pnts.drop('rasterval_buff', axis=1).to_file(os.path.join(out_dir, out_name))
# pnts.to_file(os.path.join(out_dir, out_name))
