"""
Relief indicator naar CLO tekst en data
Hans Roelofsen, WEnR, september 2021

Input CBS 500m grid (non-WEnR versie, dus met onduidelijke IDs) met relief getallen (zie: w:\PROJECTS\Landschapsmonitor\cIndicatoren\Relief\a_geodata\Format_Reliëf.pdf)
Provincie Shapefile

Analyse:
Per provincie: totaal hectare AGR_OPP_AANPERC (Oppervlakte AAN-percelen in deze CBS 500*500 rastercel)
               percentage AGR_OPP_AANPERC ten opzichte van provincie oppervlak
               totaal hectare AGR_OPP_MUTATIES_M2 (Som van de totale veranderingen in hoogte. Hierbij wordt de AAN-oppervlakte van de verschillende klassen (stijgingen en dalingen) gesommeerd)
               percentage AGR_OPP_MUTATIES_M2 ten opzichte van provincie oppervlak
               verhouding AGR_OPP_MUTATIES_M2/AGR_OPP_AANPERC in percentage

               totaal hectare NAT_OPP_NATUURLIJKTERREIN (Oppervlakte natuurlijk terrein in NatuurBeheerPlannen van de provincies in deze CBS 500*500 rastercel)
               percentage NAT_OPP_NATUURLIJKTERREIN ten opzichte van provincie oppervlak
               totaal hectare NAT_OPP_MUTATIES_M2 (Som van de totale veranderingen in hoogte. Hierbij wordt de natuurlijk terrein-oppervlakte van de verschillende klassen (stijgingen en dalingen) gesommeerd)
               percentage NAT_OPP_MUTATIES_M2 ten opzichte van provincie oppervlak
               verhouding NAT_OPP_MUTATIES_M2/NAT_OPP_NATUURLIJKTERREIN
"""

import pandas as pd
import geopandas as gp
import os
import datetime

out_dir = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Relief\h_compendium_teksten'
excel_out = r'IndicatorReliefCompendiumSamenvatting_metwater.xlsx'

relief_gdb = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\Relief\a_geodata\IndicatorRelief_30_augo_2021\IndicatorRelief_20210325.gdb'
relief_fc = 'NL_500x500'
provs_src = r'w:\PROJECTS\Landschapsmonitor\cIndicatoren\zOverigeGeodata\provincies\incl_water\provincies_2018.shp'

relief = gp.read_file(relief_gdb, layer=relief_fc)
provs = gp.read_file(provs_src)
prov_naam = 'provincien'
provs.index = provs.provincien

if not hasattr(provs, 'area_ha'):
    provs['area_ha'] = provs.area.divide(10000)

# Koppel elke 500m cell aan een Provincie
foo = gp.sjoin(relief, provs.loc[:, [prov_naam, 'geometry']], how='left')

# Drop grid cells zonder Provincie
foo.dropna(subset=[prov_naam], inplace=True)

# Some hokken may be coupled to > 1 provincie. Undo this
if len(set(foo.VIERKANT_ID)) < foo.shape[0]:
    foo.drop_duplicates(subset=['VIERKANT_ID'], inplace=True)

# Pivot to summarize per province
piv = pd.pivot_table(data=foo, index=prov_naam, values=['AGR_OPP_AANPERC', 'AGR_OPP_MUTATIES_M2', 'NAT_OPP_NATUURLIJKTERREIN',
                                                            'NAT_OPP_MUTATIES_M2'], aggfunc='sum')

# convert sq meters to hectares
piv = piv.divide(10000)
piv.rename(columns={'AGR_OPP_MUTATIES_M2': 'AGR_OPP_MUTATIES_HA',
                    'NAT_OPP_MUTATIES_M2': 'NAT_OPP_MUTATIES_HA',
                    'AGR_OPP_AANPERC': 'AGR_OPP_AANPERC_HA',
                    'NAT_OPP_NATUURLIJKTERREIN': 'NAT_OPP_NATUURLIJKTERREIN_HA'}, inplace=True)

# Join provincie areas in hectare
piv = piv.join(other=provs.area_ha, how='left')

# Bereken oppervlaktes in precentage fracties mutaties
piv['AGR_OPP_AANPERC_PERC'] = piv.AGR_OPP_AANPERC_HA.divide(piv.area_ha).multiply(100)
piv['AGR_OPP_MUTATIES_PERC'] = piv.AGR_OPP_MUTATIES_HA.divide(piv.AGR_OPP_AANPERC_HA).multiply(100)

piv['NAT_OPP_NATUURLIJKTERREIN_PERC'] = piv.NAT_OPP_NATUURLIJKTERREIN_HA.divide(piv.area_ha).multiply(100)
piv['NAT_OPP_MUTATIES_PERC'] = piv.NAT_OPP_MUTATIES_HA.divide(piv.NAT_OPP_NATUURLIJKTERREIN_HA).multiply(100)

# piv['NIET_NAT_OF_AGR_PERC'] = 100 - (piv.AGR_OPP_AANPERC_PERC + piv.NAT_OPP_NATUURLIJKTERREIN_PERC)
# piv['NIET_NAT_OF_AGR_HA'] = piv.area_ha - (piv.AGR_OPP_AANPERC_HA + piv.NAT_OPP_NATUURLIJKTERREIN_HA)

# Write to output excel
with pd.ExcelWriter(os.path.join(out_dir, excel_out), mode='w') as writer:

    metadata = {'sourcedata': os.path.join(relief_gdb, relief_fc),
                'provincie source': provs_src,
                'made by': os.environ.get('USERNAME'),
                'made when': datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S'),
                'made with': 'https://git.wur.nl/roelo008/landschapsmonitor/-/blob/master/sample/relief/relief2clo.py',
                'NOTE': 'Provincie totaal arealen zijn van nauwkeurig GIS bestand. Indicator arealen zijn van 500m blokken. Dat veroorzaakt totalen != 100%'}
    pd.DataFrame(data=metadata, index=[0]).T.to_excel(writer, sheet_name='Metadata', index=True)

    piv.loc[:, piv.columns.str.endswith('HA') | piv.columns.str.startswith('area_')]\
        .to_excel(writer, sheet_name='Statistiek_hectares', index=True)

    # piv.loc[:, piv.columns.str.endswith('PERC') | piv.columns.str.startswith('area_')]\
    #     .to_excel(writer, sheet_name='Statistiek_precentage', index=True)

