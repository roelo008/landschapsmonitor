"""
Functions and classes
"""

import numpy as np


def vec_translate(a, my_dict):
    """
    Function to apply my_dict to each element in in array a. See:
    https://stackoverflow.com/questions/16992713/translate-every-element-in-numpy-array-according-to-key

    Note that key errors are not caught!
    """
    return np.vectorize(my_dict.__getitem__)(a)


