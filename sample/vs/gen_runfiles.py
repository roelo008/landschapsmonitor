"""
Generate Viewscape runfiles *.ini
Hans Roelofsen, 18 augustus 2021
"""

import os
import argparse
import configparser
import numpy as np

parser = argparse.ArgumentParser(description='Generate INI files for Viewscape. Input datafiles must reside in DataFiles dir',
                                 allow_abbrev=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--np', help='number of points in viewpoints SHP (pnts_100m_v2021.shp)', type=int, default=4210049)
parser.add_argument('--nb', help='number of ini files to create', type=int, default=100)
parser.add_argument('--projectname', help='project settings ini file name', type=str, default='Param_aug2011.ini')
parser.add_argument('--casename', help='?', default='ViewScape01')
parser.add_argument('--viewpointsfilename', help='viewpoints SHP', default='pnts_100m_v2021.shp')
parser.add_argument('--landscapegridname', help='opgaande elementen FLT', default='gesloten_2017.flt')
parser.add_argument('--typeheighttablename', help='DBF w height per LU class', default='Hoogten12.dbf')
parser.add_argument('--elevationgridname', help='DTM flt',  default=r'terrein_h2o.flt')
parser.add_argument('--countviewgridname', help='output name for COUNT FLT', default='count')
parser.add_argument('--horizontablename', help='output name for HORIZON SHP', default='horizon')
parser.add_argument('--out_dir', help='directory 4 writing *.inis', default='\.')
parser.add_argument('--ini_name', help='ini file basename', default='ViewScape')
args = parser.parse_args()

'SHARED_DIR\DataFiles'
'WORKING_DIR'

basic = {
    'projectname': 'SHARED_DIR\DataFiles\{}'.format(args.projectname),
    'casename': args.casename,
    'minid': "",
    'maxid': "",
    'dogridfilematrix': "1",
    'viewpointsfilename': 'SHARED_DIR\DataFiles\{}'.format(args.viewpointsfilename),
    'landscapegridname': 'SHARED_DIR\DataFiles\{}'.format(args.landscapegridname),
    'typeheighttablename': 'SHARED_DIR\DataFiles\{}'.format(args.typeheighttablename),
    'elevationgridname': 'SHARED_DIR\DataFiles\{}'.format(args.elevationgridname),
    'rawdatatablename': "<NONE>",
    'countviewgridname': 'WORKING_DIR\{}_OUTPUT_NAME.flt'.format(args.countviewgridname),  # output
    'horizontablename': 'WORKING_DIR\{}_OUTPUT_NAME.shp'.format(args.horizontablename),  # output
    'landscapeareatablesname': "<NONE>"}

# Create output dir if needed
if not os.path.isdir(args.out_dir):
    os.mkdir(args.out_dir)

# Specify number of points
n_ids = args.np
start_id = 1
end_id = args.np + 1 
n_batches = args.nb

# Ini minid and maxid gelden als VANAF minid TOT maxid
ids = np.array(range(start_id, (end_id+1), 1))
batches = np.array_split(ids, n_batches)
startings = [b[0] for b in batches]
endings = [b[-1]+1 for b in batches[:-1]] + [batches[-1][-1]]
end_start = list(zip(startings, endings))

# Double check
assert end_start[0][0] == start_id
assert end_start[-1][-1] == end_id
assert len(range(end_start[0][0], end_start[-1][-1])) == n_ids

for x in end_start:
    start, end = x
    fname = '{}_{:07}-{:07}.ini'.format(args.ini_name, start, end)

    basic.update(minid=str(start),
                 maxid=str(end))
    ini = configparser.ConfigParser()
    ini['ViewShed01'] = basic

    with open(os.path.join(args.out_dir, fname), 'w') as configfile:
        ini.write(configfile)

print('...done')






