$connectTestResult = Test-NetConnection -ComputerName viewscapediag.file.core.windows.net -Port 445
if ($connectTestResult.TcpTestSucceeded) {
    # Save the password so the drive will persist on reboot
    cmd.exe /C "cmdkey /add:`"viewscapediag.file.core.windows.net`" /user:`"localhost\viewscapediag`" /pass:`"WaxZFarUCCOvJPdzH36uGroOMYwqOBzpEkk7vudY2K8dhryp0rihBPtNXgrIO8s4xChT8axaDVAWkRBdnbgohQ==`""
    # Mount the drive
    New-PSDrive -Name Q -PSProvider FileSystem -Root "\\viewscapediag.file.core.windows.net\viewscapedata" -Persist
} else {
    Write-Error -Message "Unable to reach the Azure storage account via port 445. Check to make sure your organization or ISP is not blocking port 445, or use Azure P2S VPN, Azure S2S VPN, or Express Route to tunnel SMB traffic over a different port."
}