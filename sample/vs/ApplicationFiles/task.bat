REM Task file for Batch Viewscape running
REM CLI arguments %1 input file path (ini file)
REM               %2 output ID nr
REM               %3 output file path

cd %AZ_BATCH_TASK_WORKING_DIR%

REM Find and Replace in the INI file
fart -i -r %1 SHARED_DIR %AZ_BATCH_NODE_SHARED_DIR%
fart -i -r %1 WORKING_DIR %AZ_BATCH_TASK_WORKING_DIR%
fart -i -r %1 OUTPUT_NAME %2

REM 7za.exe a -r -t7z -m0=LZMA2:d64k:fb32 -ms=8m -mmt=30 -mx=9 "%AZ_BATCH_TASK_WORKING_DIR%\%3" "%AZ_BATCH_TASK_WORKING_DIR%\*.*"
ViewScapeCon.exe %AZ_BATCH_TASK_WORKING_DIR%\%1 
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& 'DriveConnect.ps1'"

xcopy /e /i /y /q  %AZ_BATCH_TASK_WORKING_DIR%\*.* "\\viewscapediag.file.core.windows.net\viewscapedata\!Results\*.*"


